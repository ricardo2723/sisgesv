<li class="active"><a href="{{ route('gestor.dashboard') }}"><i class="fa fa-dashboard"></i> <span>DashBoard </span></a></li>
<li class="treeview">
            <a href="#">
              <i class="fa fa-check-circle-o"></i> <span>Relatórios Serviço</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>

    <ul class="treeview-menu">
        <li><a href="{{ route('gestor.reportServico') }}" ><i class="fa fa-calendar"></i> <span>Serviço por Data </span></a></li>
        <li><a href="{{ route('gestor.reportServicoOficial') }}"><i class="fa fa-star"></i> <span>Serviço por Oficial de Dia </span></a></li>
        <li><a href="{{ route('gestor.reportServicoCmtGda') }}"><i class="fa fa-user-o"></i> <span>Serviço por Cmt Gda</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-car"></i> <span>Relatórios Viatura</span>
        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
    </a>

    <ul class="treeview-menu">
        <li><a href="{{ route('gestor.reportViatura') }}" ><i class="fa fa-calendar"></i> <span>Viatura por Data </span></a></li>
        <li><a href="{{ route('gestor.reportServicoOficial') }}"><i class="fa fa-star"></i> <span>Viatura por Oficial de Dia </span></a></li>
        <li><a href="{{ route('gestor.reportServicoCmtGda') }}"><i class="fa fa-user-o"></i> <span>Viatura por Cmt Gda</span></a></li>
    </ul>
</li>
<li style="background-color: #9fa000">
    <a href="#"
       onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
        <i class="fa fa-external-link"></i> <span> Sair </span></a>
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</li>
