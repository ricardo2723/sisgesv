         
  <li class="active"><a href="{{ route('registroViatura.dashboard') }}"><i class="fa fa-dashboard"></i> <span>DashBoard </span></a></li>
  <li><a href="{{ route('registroViaturas.index') }}"><i class="fa fa-car"></i> <span> Fichas Viaturas </span></a></li>
  <li><a href="#"><i class="fa fa-male"></i> <span> Fichas Visitantes </span></a></li>
  
  
  <li style="background-color: #9fa000">
    <a href="#"
      onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
      <i class="fa fa-sign-out"></i> <span> Sair </span></a>
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
      </form>  
    </li>    
