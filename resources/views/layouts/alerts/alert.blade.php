<script>
    @if(session('success') != null)
        @if(session('success')['success'])
            $(document).ready(function(){
                swal("Sucesso", "{{ session('success')['messages'] }}", "success",
                {                         
                    buttons: false,
                    timer: 2000,
                })
            });                
        @else    
            $(document).ready(function(){
                swal("Atenção", "{{ session('success')['messages'] }}", "error");
            });
        @endif
    @endif
</script>