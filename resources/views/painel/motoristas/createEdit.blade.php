@extends('layouts.mastertop')

@section('content')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-xs-12 col-md-6">
            <div id="result"></div>
            <div class="box box">
                <div style="background-color: #808000; color:beige" class="box-header with-border">
                    @if(isset($motorista))
                        <h3><i class="fa fa-car"></i> Alterar Motorista | Chefe de Viatura</h3>
                        {!! Form::model($motorista, ['route' => ['motoristas.update', $motorista->id], 'method' => 'put']) !!}
                    @else
                        <h3><i class="fa fa-car"></i> Cadastrar Motorista | Chefe de Viatura</h3>
                        {!! Form::open(['route' => 'motoristas.store', 'method' => 'post']) !!}
                    @endif
                    <h5>Registrar todos os Motoristas e Chefes de Viatura da OM.</h5>
                </div>

                @include('layouts.alerts.validationAlert')
                <div class="box-body">

                        <div class="form-group">
                            <label>Nome Completo</label>
                            {!! Form::text('nome_completo', $value = null, ['class' => 'form-control', 'autofocus']) !!}
                        </div>

                        <div class="form-group">
                            <label>Nome de Guerra</label>
                            {!! Form::text('nome_guerra', $value = null, ['class' => 'form-control', 'placehold' => 'Nome de Guerra sem espaço']) !!}
                        </div>

                        <div class="form-group">
                            <label>Cpf</label>
                            {!! Form::text('cpf', $value = null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            <label>Função</label>
                            {!! Form::select(
                                    'funcao',
                                    [
                                        'Motorista'         => 'Motorista',
                                        'Chefe de Viatura'  => 'Chefe de Viatura',
                                    ],
                                    null,
                                    ['class' => 'form-control flat select2', 'style' => 'width: 100%']
                                )
                            !!}
                        </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit(isset($registroViaturaEdit) ? 'Alterar' : 'Cadastrar', ['class' => 'btn flat btn-oliva']) !!}
                    <a href="{{ route('motoristas.index') }}"
                       class="btn btn-oliva flat">Fechar</a>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
@endpush

@push('datatables-script')
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>

        $(document).ready(function () {
            $('#hora').inputmask("h:s", {"mask": "99:99"}); //specifying options            
        });

    </script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })
    </script>
@endpush 