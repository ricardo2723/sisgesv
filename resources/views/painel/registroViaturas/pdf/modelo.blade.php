
@section('content')

    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: black;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: black;
        }

        .tg .tg-g145 {
            font-family: "Times New Roman", Times, serif !important;;
            text-align: center;
            vertical-align: top;
            vertical-align: middle
        }

        .tg .tg-gh1y {
            font-weight: bold;
            font-family: "Times New Roman", Times, serif !important;;
            text-align: center;
            vertical-align: middle
        }

        .tg .tg-gh1h {
            font-weight: bold;
            font-family: "Times New Roman", Times, serif !important;;
            text-align: center;
            vertical-align: middle;
        }

        .tg .tg-z7id {
            font-size: 12px;
            font-family: "Times New Roman", Times, serif !important;;
            text-align: center;
            vertical-align: top;
            padding: 10px 20px;
        }

        .tg .tg-hu3l {
            font-weight: bold;
            font-size: 100%;
            font-family: "Times New Roman", Times, serif !important;;
            text-align: center;
            vertical-align: top;
        }

        .tg .tg-x4jz {
            font-weight: bold;
            font-family: "Times New Roman", Times, serif !important;;
            text-align: center;
            padding: 3px 10px
        }
    </style>



    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3><i class="fa fa-database"></i> Relatório Diário do Serviço</h3>
                    Viaturas que entraram e sairam da OM
                    <a class="btn btn-primary btn-flat btn-danger pull-right"
                       href="{{ isset($id) ? route( $route , $id) : route($route) }}" target=_blank><i
                                class="fa fa-file-pdf"></i> Gerar PDF</a>
                </div>
                <div class="box-body">

                    <table class="tg" width="100%">
                        <tr>
                            <th class="tg-gh1h" width="100">VISTO:<br><br>{{ $viaturas->user->username }}<br>____________<br>Of
                                Dia
                            </th>
                            <th class="tg-z7id" colspan="4"><span
                                        style="font-weight:bold">MINISTÉRIO DA DEFESA </span><br><span
                                        style="font-weight:bold">EXÉRCITO BRASILEIRO</span><br><span
                                        style="font-weight:bold">7º BATALHÃO DE ENGENHARIA DE COMBATE </span><br><span
                                        style="font-weight:bold">(Batalhão de Engenheiros / 1855)</span><br><span
                                        style="font-weight:bold;text-decoration:underline">BATALHÃO VISCONDE DE TAUNAY</span>
                            </th>
                            <th class="tg-hu3l" colspan="5"><br>CONTROLE DO MOVIMENTO DE VIATURAS <br>DO 7º
                                BECMB
                                <br><br>Serviço do Dia {{ $viaturas->formatted_data_inicio_dia }} para o
                                dia {{ $viaturas->formatted_data_final_dia }}
                                de {{ $viaturas->formatted_data_final_mes }} de 2018<br></th>
                            <th class="tg-gh1h" width="100">VISTO:<br><br>{{ $viaturas->userCmt->username }}<br>____________<br>Cmt
                                Gda
                            </th>
                        </tr>
                        <tr>
                            <td class="tg-x4jz" colspan="3">DADOS DA VIATURA</td>
                            <td class="tg-x4jz" width="70" rowspan="2">MOTORISTA</td>
                            <td class="tg-x4jz" colspan="2">SAÍDA</td>
                            <td class="tg-x4jz" colspan="2">ENTRADA</td>
                            <td class="tg-x4jz" rowspan="2">DESTINO</td>
                            <td class="tg-x4jz" rowspan="2" colspan="2">CHEFE VIATURA</td>
                        </tr>
                        <tr>
                            <td class="tg-gh1y">Prefixo</td>
                            <td class="tg-gh1y" width="53">EB/Placa</td>
                            <td class="tg-gh1y" width="35">Cia</td>
                            <td class="tg-gh1y" width="50">Km</td>
                            <td class="tg-gh1y" width="100">Data</td>
                            <td class="tg-gh1y" width="50">Km</td>
                            <td class="tg-gh1y" width="37">Data</td>
                        </tr>
                        @if(isset($servicoViaturas))
                            @foreach ($servicoViaturas as $viatura)
                                <tr>
                                    <td class="tg-g145" width="50">{{ $viatura->detalhesViatura->prefixo }}</td>
                                    <td class="tg-g145"
                                        width="70">{{ $viatura->detalhesViatura->eb_placa }}</td>
                                    <td class="tg-g145" width="50">{{ $viatura->cia }}</td>
                                    <td class="tg-g145" width="150">{{ $viatura->motorista }}</td>
                                    <td class="tg-g145" width="60">{{ $viatura->odom_saida }}</td>
                                    <td class="tg-g145"
                                        width="110">{{ $viatura->formatted_odom_saida_data . ' ' . $viatura->formatted_odom_saida_hora }}</td>
                                    @if(!isset($viatura->entradaViatura->odom_entrada_data))
                                        <td class="tg-g145" colspan="2" width="60"> Não Retornou</td>
                                    @else
                                        <td class="tg-g145"
                                            width="60">{{ $viatura->entradaViatura->odom_entrada }}</td>
                                        <td class="tg-g145"
                                            width="110">{{ $viatura->entradaViatura->formatted_odom_entrada_data . ' ' . $viatura->entradaViatura->formatted_up_odom_entrada_hora }}</td>
                                    @endif
                                    <td class="tg-g145" width="250">{{ $viatura->destino }}</td>
                                    <td class="tg-g145" colspan="2"
                                        width="100">{{ $viatura->chefe_viatura }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

<script>
    $('#navbar-topCasaFina').on('show.collapse', function () {
        $('.topCasaFina-banner').css('transform', 'translate(-50%, 10%)');
    })
</script>
