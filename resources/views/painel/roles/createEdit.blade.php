@extends('layouts.mastertop')

@section('content')
<div class="row">
                <div class="col-md-3"></div>
                <div class="col-xs-12 col-md-6">
                        <div id="result"></div>
                        <div class="box box">
                                <div style="background-color: #808000; color:beige" class="box-header with-border">
                                    @if(isset($role))        
                                        <h3><i class="fa fa-unlock"></i> Alterar Perfil de Acesso</h3>
                                        {!! Form::model($role,['route' => ['acessos.update', $role->id], 'method' => 'PUT']) !!}                                        
                                        <h5>Alterar o perfil de acesso.</h5>
                                    @else
                                        <h3><i class="fa fa-unlock"></i> Cadastrar o Perfil de Acesso</h3>
                                        {!! Form::open(['route' => ['acessos.store'], 'method' => 'POST']) !!}
                                        <h5>Cadastrar o perfil de acesso.</h5>
                                    @endif                                    
                                    </div>
                                    @include('layouts.alerts.validationAlert')                               
                                <div class="box-body">
                                    <div class="rows">
                                        <div class="form-group">
                                            <label for="name">Nome: </label>
                                            {!! Form::text('name', $value = null, ['class' => 'form-control', 'autofocus']) !!}
                                        </div>
                                        <div class="form-group">
                                            <label for="slug">Acesso: </label>
                                            @if(isset($role) && $role->id === 1) 
                                            {!! Form::text('slug', $value = null, ['class' => 'form-control', 'readonly' ]) !!}    
                                            @else
                                                {!! Form::text('slug', $value = null, ['class' => 'form-control']) !!}
                                            @endif
                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="description">Descrição: </label>
                                            {!! Form::textarea('description', $value = null, ['class' => 'form-control', 'rows' => '3']) !!}
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="rows">
                                        <h4><i class="fa fa-ulock"></i> Permissão Especial</h4>
                                        <div class="form-group">
                                                <label>{!! Form::radio('special','all-access', null) !!} Acesso Total</label>
                                                @if(isset($role) && $role->id != 1)
                                                    <label>{!! Form::radio('special','no-access', null) !!} Sem Acesso</label>
                                                @else
                                                    <label>{!! Form::radio('special','no-access', null) !!} Sem Acesso</label>
                                                @endif
                                            </div>
                                    </div>
                                    <hr>
                                     <div class="rows">
                                        <h3>Lista de Permissões</h3>  
                                        <ul class="list-unstyled">                                 
                                            @foreach ($permissions as $permission)
                                                <li>    
                                                    <label>
                                                        {!! Form::checkbox('permissions[]', $permission->id, null) !!}
                                                        {{ $permission->name }} - <em>({{ $permission->description }})</em>
                                                    </label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>                                
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    {!! Form::submit(isset($role) ? 'Alterar' : 'Cadastrar', ['class' => 'btn btn-oliva flat']) !!}
                                    <a href="{{ route('acessos.index') }}" class="btn btn-oliva flat">Voltar</a>
                                </div>
                                {!! Form::close() !!}
                        </div><!-- /.box -->
                    </div><!-- /.col -->           
                </div>
@endsection
@push('datatables-css')
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
@endpush

@push('datatables-script')
<!-- iCheck 1.0.1 -->
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
@endpush
