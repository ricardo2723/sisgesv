@extends('layouts.mastertop')

@section('content')
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3><i class="fa fa-user"></i> Manutenção de Usuários</h3>
                                Usuários Cadastrados no Sistema
                                {{-- <a class="btn btn-primary btn-flat btn-oliva pull-right"
                                href="{{ route('register') }}"><i class="fa fa-user"></i> Cadastrar Usuário</a> --}}
                            </div>
                            <div class="box-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead class="table_blue">
                                    <tr>
                                        <th>Id</th>
                                        <th>Nome</th>
                                        <th>Login</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->username }}</td>
                                            <td style="text-align: center;">
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('usuarios.edit', $user->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Manutenção de Usuários"><i
                                                                class="fa fa-user"></i></a>
                                                </div>
                                                <div style="margin-right:5px" class="btn-group">
                                                    <a href="{{ route('usuarios.show', $user->id) }}"
                                                       data-toggle="tooltip" data-placement="top"
                                                       title="Excluir Usuário"> <i
                                                                class='fa fa-trash text-red'></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.col -->
                </div><!-- /.row -->            
@endsection

@push('datatables-css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('adminlte/components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@push('datatables-script')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('pikeadmin/js/sweetalertsaw.min.js') }}"></script>

    <script>
        $(function () {
            $("#example1").DataTable(
                {
                    "language": {
                        "url": "/adminlte/components/datatables.net/js/ptBr.lang"
                    }
                }
            )
        });
    </script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>    
@endpush