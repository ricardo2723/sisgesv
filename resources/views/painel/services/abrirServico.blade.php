@extends('layouts.mastertop')

@section('content')

<!-- Modal -->
<div class="modal fade custom-modal" data-backdrop="static" id="customModal" tabindex="1" role="dialog" aria-labelledby="customModal" aria-hidden="true">
    <div class="modal-dialog" style="width: 35em">
        <div class="modal-content">
            <div class="modal-header">
            <h3><i class="fa fa-key"></i> Abertura do Serviço</h3>
            {!! Form::open(['route' => 'servico.store', 'method' => 'post']) !!}
            <h5>Definir Cmt Gda e Data de Inicio do Serviço</h5>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    @include('layouts.alerts.validationAlert')
                    <div class="col-xs-12">
                    <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <div class="form-group">                                                
                                            <label>Comandante da Guarda</label>                                            
                                            {!! Form::hidden('user_id', $user_id) !!}                                            
                                            {!! Form::select('cmt_user_id', isset($users) ? $users : ['0' => 'Vazio'] , null, ['class' => 'form-control flat select2', 'style' => 'width: 100%']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="form-group">
                                            <label>Data de Inicio</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                                </div>                                                 
                                                {!! Form::date('data_inicio', $value = null, ['class' => 'form-control']) !!}
                                            </div>                                            
                                            <!-- /.input group -->
                                        </div>
                                    </div>                                                                       
                                </div>                                
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <div class="form-group"> 
                                        {!! Form::submit('Abrir Serviço', ['class' => 'btn flat btn-oliva']) !!}
                                        <a href="{{ route('login') }}" class="btn btn-oliva flat">Sair</a>                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}         
        </div>
    </div>
</div>

@endsection

@push('datatables-css')
    <link rel="stylesheet" href="{{ asset('adminlte/components/select2/dist/css/select2.min.css') }}"> 
    <link rel="stylesheet" href="{{ asset('pikeadmin/css/style.css') }}">
@endpush

@push('datatables-script')
    <script src="{{ asset('pikeadmin/components/popper/popper.min.js') }}"></script>
    <script src="{{ asset('adminlte/components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>    
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
        })        
    </script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <script>
        $(document).ready(function () {
            $('#customModal').modal('show');
        });
    </script>
@endpush