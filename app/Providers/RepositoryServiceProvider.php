<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\ViaturasRepository::class, \App\Repositories\ViaturasRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RegistroViaturasRepository::class, \App\Repositories\RegistroViaturasRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RegistroViaturaEntradasRepository::class, \App\Repositories\RegistroViaturaEntradasRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RegistroViaturaOficialRepository::class, \App\Repositories\RegistroViaturaOficialRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ServiceRepository::class, \App\Repositories\ServiceRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MotoristasRepository::class, \App\Repositories\MotoristasRepositoryEloquent::class);
        //:end-bindings:
    }
}
