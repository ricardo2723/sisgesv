<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistroViaturaEntradasCreateRequest;
use App\Http\Requests\RegistroViaturaEntradasUpdateRequest;
use App\Repositories\RegistroViaturasRepository;
use App\Services\RegistroViaturaEntradasService;
use Validator;

class RegistroViaturaEntradasController extends Controller
{
    protected $repositoryRegistroViatura;
    protected $service;

    public function __construct(RegistroViaturasRepository $repositoryRegistroViatura, RegistroViaturaEntradasService $service)
    {
        $this->repositoryRegistroViatura   = $repositoryRegistroViatura;
        $this->service                      = $service;
    }

    public function show($id)
    {
        $registroViaturaEntrada = $this->repositoryRegistroViatura->find($id);

        return view('painel.registroViaturas.entradas.closeRegistroViatura', compact('registroViaturaEntrada'));
    }

    public function store(RegistroViaturaEntradasCreateRequest $request)
    {
        $request = $this->service->store($request->all());

        session()->flash('success',[
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('registroViaturas.index');
    }

    public function update(RegistroViaturaEntradasUpdateRequest $request, $id)
    {
        $request = $this->service->update($request->all(), $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('records.index');
    }
}
