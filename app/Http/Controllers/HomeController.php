<?php

namespace App\Http\Controllers;


class HomeController extends Controller
{
    
    public function index()
    {
        if(isset(\Auth::user()->roles[0]->slug) && \Auth::user()->roles[0]->slug == 'ofDia')
        {
            return redirect()->route('registroViaturaOficial.dashboard');
        }
        
        if(isset(\Auth::user()->roles[0]->slug) && \Auth::user()->roles[0]->slug == 'cmtGda')
        {
            return redirect()->route('registroViatura.dashboard');
        }

        if(isset(\Auth::user()->roles[0]->slug) && \Auth::user()->roles[0]->slug == 'admin')
        {
            return view('painel.home.index');
        }

        if(isset(\Auth::user()->roles[0]->slug) && \Auth::user()->roles[0]->slug == 'gestor')
        {
            return redirect()->route('gestor.dashboard');
        }

        return view('painel.home.index');
    }
}
