<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Entities\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Exceptions\ExceptionsErros;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $erros;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ExceptionsErros $erros)
    {
        $this->middleware('guest');
        $this->erros = $erros;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'name.required'      => 'O Nome e Obrigatório',
            'name.min'           => 'O Nome deve ter no minimo 3 caracteres',
            'name.max'           => 'O Nome deve ter no maximo 100 caracteres',
            'username.required'  => 'O Login e Obrigatório',
            'username.min'       => 'O Login deve ter no minimo 5 caracteres',
            'username.max'       => 'O Login deve ter no maximo 30 caracteres',
            'username.unique'    => 'O Login já está existe no banco de Dados',
            'password.required'  => 'A Senha e Obrigatória',
            'password.min'       => 'A Senha deve ter no minimo 6 caracteres',
            'password.confirmed' => 'As senhas não coincidem',
        ];

        return Validator::make($data, [
            'name' => 'required|string|min:3|max:100',
            'username' => 'required|string|min:3|max:30|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ], $messages);
    }


    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        try
        {
            User::create([
                'name' => $request['name'],
                'username' => $request['username'],
                'password' => bcrypt($request['password']),
            ]);

            session()->flash('success', [
                'success'   => true,
                'messages'  => 'Usuário Cadastrado com Sucesso. Ative o seu cadastro na STI.'
            ]);
            return redirect()->route('login');

        }catch(\Exception $e)
        {
            return $this->erros->errosExceptions($e);
        }
        
    }  
}
