<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use App\Services\RolesService;
use App\Http\Requests\RoleCreateRequest;
use App\Http\Requests\RoleUpdateRequest;

class RolesController extends Controller
{
    protected $service;
    public function __construct(RolesService $service)
    {
        $this->middleware('permission::admin');        
        $this->service = $service;
    }

    public function index()
    {
        $roles = Role::all();

        return view('painel.roles.index', compact('roles'));
    }

    public function create()
    {
        $permissions = Permission::get();

        return view('painel.roles.createEdit', compact('permissions'));
    }

    public function store(RoleCreateRequest $request)
    {
        $role = $this->service->store($request);

        session()->flash('success', [
            'success' => $role['success'],
            'messages' => $role['messages'],
        ]);

        return redirect()->route('acessos.index');
    }

    Public function show($id)
    {
        $role = Role::find($id);
        return view('painel.roles.show', compact('role')); 
    }
    
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = Permission::get();
        
        return view('painel.roles.createEdit', compact('role', 'permissions'));
    }

    public function update(RoleUpdateRequest $request, $id)
    {
        $role = $this->service->update($request, $id);

        session()->flash('success', [
            'success'   => $role['success'],
            'messages'  => $role['messages']
        ]);

        return redirect()->route('acessos.index');
    }

    public function destroy($id)
    {
        $request = $this->service->destroy($id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages'],
        ]);
        
        return redirect()->route('acessos.index');
    }
}
