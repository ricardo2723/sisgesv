<?php

namespace App\Http\Controllers;

use App\Services\MotoristasService;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\MotoristasCreateRequest;
use App\Http\Requests\MotoristasUpdateRequest;
use App\Repositories\MotoristasRepository;

/**
 * Class MotoristasController.
 *
 * @package namespace App\Http\Controllers;
 */
class MotoristasController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct(MotoristasRepository $repository, MotoristasService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $motoristas = $this->repository->all();
        return view('painel.motoristas.index', compact('motoristas'));
    }

    public function create()
    {
        return view('painel.motoristas.createEdit');
    }

    public function store(MotoristasCreateRequest $request)
    {
        $motorista = $this->service->store($request->all());

        session()->flash('success', [
            'success'   => $motorista['success'],
            'messages'  => $motorista['messages']
        ]);

        return redirect()->route('motoristas.index');
    }

    public function edit($id)
    {
        $motorista = $this->repository->find($id);

        return view('painel.motoristas.createEdit', compact('motorista'));
    }

    public function update(MotoristasUpdateRequest $request, $id)
    {
        $motorista = $this->service->update($request->all(), $id);

        session()->flash('success', [
           'success'    => $motorista['success'],
           'messages'  => $motorista['messages']
        ]);

        return redirect()->route('motoristas.index');
    }

    public function show($id)
    {
        $motorista = $this->repository->find($id);

        return view('painel.motoristas.show', compact('motorista'));
    }

    public function destroy($id)
    {
        $motorista = $this->service->destroy($id);

        session()->flash('success', [
            'success'   => $motorista['success'],
            'messages'  => $motorista['messages'],
        ]);

        return redirect()->route('motoristas.index');
    }
}
