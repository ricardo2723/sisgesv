<?php

namespace App\Http\Controllers;

use App\Repositories\MotoristasRepository;
use App\Repositories\ViaturasRepository;
use App\Services\RegistroViaturasService;
use App\Services\RegistroViaturaEntradasService;
use App\Http\Requests\RegistroViaturasCreateRequest;
use App\Http\Requests\RegistroViaturasUpdateRequest;
use App\Http\Requests\RegistroViaturaOficialUpdateRequest;
use App\Repositories\RegistroViaturasRepository;
use App\Entities\Service;
use App\Services\ServicesService;

class RegistroViaturasController extends Controller
{
    protected $repository;
    protected $viaturaRepository;
    protected $service;
    protected $serviceEntrada;
    protected $servicoService;
    protected $motoristaRepository;

    public function __construct(RegistroViaturasRepository $repository, ViaturasRepository $viaturaRepository,
                                RegistroViaturaEntradasService $serviceEntrada, RegistroViaturasService $service,
                                ServicesService $servicoService, MotoristasRepository $motoristaRepository)
    {
        $this->middleware('permission:cmtgda');
        $this->repository           = $repository;
        $this->viaturaRepository    = $viaturaRepository;
        $this->service              = $service;
        $this->serviceEntrada       = $serviceEntrada;
        $this->servicoService       = $servicoService;
        $this->motoristaRepository       = $motoristaRepository;
    }

    public function dashboard()
    {
        return view('painel.dashboard.cmtgda.dashboard', 
                $this->servicoService->dadosServicoDashboard(),
                $this->service->dadosViaturaDashboard('cmt_user_id')
            );
    }

    public function index()
    {
        $registroViaturas = $this->repository->findWhereNotIn('status', [4]);        
        
        return view('painel.registroViaturas.saida.index', compact('registroViaturas'));
    }

    public function create()
    {
        $serv = Service::where('status', 0)->where('cmt_user_id', \Auth::user()->id)->first();
        $viaturas = $this->viaturaRepository->selectListBox();
        $motorista = $this->motoristaRepository->findWhere(['funcao' =>'Motorista']);
        $chefeViatura = $this->motoristaRepository->findWhere(['funcao' => 'Chefe de Viatura']);
        return view('painel.registroViaturas.saida.createEdit', compact('viaturas', 'serv', 'motorista', 'chefeViatura'));
    }

    public function store(RegistroViaturasCreateRequest $request)
    {
        $registroViatura = $this->service->store($request->all());

        session()->flash('success', [
            'success'   => $registroViatura['success'],
            'messages'  => $registroViatura['messages']
        ]);

        return redirect()->route('registroViaturas.index');
    }

    public function edit($id)
    {
        $viaturas = $this->viaturaRepository->selectListBox();
        $registroViaturaEdit = $this->repository->find($id);

        return view('painel.registroViaturas.saida.createEdit', compact('viaturas', 'registroViaturaEdit'));
    }

    public function update(RegistroViaturasUpdateRequest $request, $id)
    {
        $serv = Service::where('status', 0)->where('cmt_user_id', \Auth::user()->id)->first();
        $request['service_id'] = $serv->id;
        $request = $this->service->update($request->all(), $id);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages']
        ]);

        return redirect()->route('registroViaturas.index');
    }

    public function show($id)
    {
        $registroViatura = $this->repository->find($id);

        return view('painel.registroViaturas.saida.show', compact('registroViatura'));
    }

    public function closeViatura($id)
    {
        $record_id = $id;
        return view('painel.registroViaturas.index', compact('record_id'));
    }

    public function destroy($id)
    {
        $delete = $this->service->destroy($id);

        session()->flash('success', [
            'success'   => $delete['success'],
            'messages'  => $delete['messages']
        ]);

        return redirect()->route('registroViaturas.index');
    }

    public function corrigirRegistroShow($id)
    {
        $corrigirRegistroViatura = $this->repository->find($id);
        $viatura_list = $this->viaturaRepository->selectListBox();

        return view('painel.registroViaturas.oficial.corrigirRegistro', compact('corrigirRegistroViatura', 'viatura_list'));
    }
    
    public function corrigirRegistroUpdate(RegistroViaturaOficialUpdateRequest $request, $id)
    {
        $corrigirRegistro = $this->service->corrigirRegistroUpdate($request->all(), $id);

        if($corrigirRegistro['success']){
            $corrigirRegistro = $this->serviceEntrada->corrigirRegistroUpdate($request->all());

            session()->flash('success', [
                'success'   => $corrigirRegistro['success'],
                'messages'  => $corrigirRegistro['messages']
            ]);
        }else
        {
            session()->flash('success', [
                'success'   => false,
                'messages'  => 'Não foi possivel Corrigir os dados de Saida da Viatura.'
            ]);
        }
        return redirect()->route('registroViaturas.index');
    }
}
