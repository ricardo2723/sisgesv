<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistroViaturasUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'cia'               => 'required',
            'motorista'         => 'required',
            'chefe_viatura'     => 'required',
            'destino'           => 'required|min:4|max:100',
            'odom_saida'        => 'digits_between:0,6|required|numeric',
            'odom_saida_data'   => 'required',
            'odom_saida_hora'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'motorista.required'        => 'O Nome do MotoristasController é Obrigatório',
            'odom_saida.required'       => 'O Odometro de Saida é obrigatório',
            'odom_saida.numeric'        => 'O Odometro de Saida deve ter somente numeros',
            'odom_saida.digits_between' => 'O Odometro de Saida deve ter no maximo 6 Digitos',
            'odom_saida_data.required'  => 'A Data da Saida é Obrigatória',
            'odom_saida_hora.required'  => 'A Hora da Saida é Obrigatória',
            'chefe_viatura.required'    => 'O Nome do Chefe de Viatura é Obrigatório',
            'destino.required'          => 'O Destino é Obrigatório',
            'destino.min'               => 'O Destino deve ter no Minimo 4 caracteres',
            'destino.max'               => 'O Destino deve ter no Maximo 100 caracteres',
        ];
    }
}
