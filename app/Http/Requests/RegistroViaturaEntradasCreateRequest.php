<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\big;

class RegistroViaturaEntradasCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {           
        return [
            'odom_entrada'        => ['digits_between:0,6',' required', 'numeric', 'big:'.$this->odom_saida],
            'odom_entrada_data'   => 'required|after_or_equal:date_in',
            'odom_entrada_hora'   => 'required',
        ];
    }       

    public function validationData()
    {
        $dados = $this->all();

        $dados['odom_entrada_data'] = $dados['odom_entrada_data'] . ' ' . $dados['odom_entrada_hora'];        

        return $dados;
    }

    public function messages()
    {
        return [
            'odom_entrada.required'             => 'O Odometro de Entrada é obrigatório',
            'odom_entrada.numeric'              => 'O Odometro de Entrada deve ter somente numeros',
            'odom_entrada.digits_between'       => 'O Odometro de Entrada deve ter no maximo 6 Digitos',
            'odom_entrada_data.required'        => 'A Data da Entrada é Obrigatória',
            'odom_entrada_data.after_or_equal'  => 'A Data da Entrada deve ser maior ou igual a data de Saida',
            'odom_entrada_hora.required'        => 'A Hora da Entrada é Obrigatória',
        ];
    }
}
