<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'data_inicio'   => 'required|date|unique:services,data_inicio',
        ];
    }

    public function validationData()
    {
        $dados = $this->all();

        $dados['data_inicio'] = $dados['data_inicio'] . ' ' . '08:00:00';        
        
        return $dados;
    }

    public function messages()
    {
        return [
            'data_inicio.required'  => 'A Data de Inicio do Serviço é Obrigatória.',
            'data_inicio.date'      => 'A Data de Inicio do Serviço não é Válida.',
            'data_inicio.unique'    => 'Já existe um serviço com esta data no banco de dados.',
        ];
    }
}
