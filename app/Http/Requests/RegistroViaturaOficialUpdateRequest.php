<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\big;
class RegistroViaturaOficialUpdateRequest extends FormRequest
{    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'cia'               => 'required',
            'motorista'         => 'required|min:6|max:45',
            'chefe_viatura'     => 'required|min:6|max:45',
            'destino'           => 'required|min:4|max:100',
            'odom_saida'        => 'digits_between:0,6|required|numeric',
            'odom_saida_data'   => 'required',
            'odom_saida_hora'   => 'required',
            'odom_entrada'      => ['digits_between:0,6',' required', 'numeric', 'big:'.$this->odom_saida],
            'odom_entrada_data' => 'required|after_or_equal:odom_saida_data',
            'odom_entrada_hora' => 'required',
        ];
    }

    public function validationData()
    {
        $dados = $this->all();

        $dados['odom_entrada_data'] = $dados['odom_entrada_data'] . ' ' . $dados['odom_entrada_hora'];        
        $dados['odom_saida_data'] = $dados['odom_saida_data'] . ' ' . $dados['odom_saida_hora'];        

        return $dados;
    }

    public function messages()
    {
        return [
            'motorista.required'                => 'O Nome do MotoristasController é Obrigatório',
            'motorista.min'                     => 'O Nome do MotoristasController deve ter no Minimo 6 caracteres',
            'motorista.max'                     => 'O Nome do MotoristasController deve ter no Maximo 45 caracteres',
            'odom_saida.required'               => 'O Odometro de Saida é obrigatório',
            'odom_saida.numeric'                => 'O Odometro de Saida deve ter somente numeros',
            'odom_saida.digits_between'         => 'O Odometro de Saida deve ter no maximo 6 Digitos',
            'odom_saida_data.required'          => 'A Data da Saida é Obrigatória',
            'odom_saida_hora.required'          => 'A Hora da Saida é Obrigatória',
            'chefe_viatura.required'            => 'O Nome do Chefe de Viatura é Obrigatório',
            'chefe_viatura.min'                 => 'O Nome do Chefe de Viatura deve ter no Minimo 6 caracteres',
            'chefe_viatura.max'                 => 'O Nome do Chefe de Viatura deve ter no Maximo 45 caracteres',
            'destino.required'                  => 'O Destino é Obrigatório',
            'destino.min'                       => 'O Destino deve ter no Minimo 4 caracteres',
            'destino.max'                       => 'O Destino deve ter no Maximo 100 caracteres',
            'odom_entrada.required'             => 'O Odometro de Entrada é obrigatório',
            'odom_entrada.gt'                   => 'O Odometro de Entrada deve ser maior que o de Saida',
            'odom_entrada.numeric'              => 'O Odometro de Entrada deve ter somente numeros',
            'odom_entrada.digits_between'       => 'O Odometro de Entrada deve ter no maximo 6 Digitos',
            'odom_entrada_data.required'        => 'A Data da Entrada é Obrigatória',
            'odom_entrada_data.after_or_equal'  => 'A Data da Entrada deve ser maior ou igual a data de Saida',
            'odom_entrada_hora.required'        => 'A Hora da Entrada é Obrigatória',
        ];
    }
}
