<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|min:8|max:100',
            'username'  => 'required|min:5|max:30|unique:users,username,' . $this->usuario, 
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => 'O Nome é Obrigatório',
            'name.min'          => 'O Nome deve ter no Minimo 8 Caracteres',
            'name.max'          => 'O Nome deve ter no Maximo 100 Caracteres',
            'username.required' => 'O Nome de Login é Obrigatório',
            'username.min'      => 'O Nome de Login deve ter no Minimo 6 Caracteres',
            'username.max'      => 'O Nome de Login deve ter no Maximo 30 Caracteres',
            'username.unique'   => 'O Nome de Login já está cadastrado no Banco de Dados',
        ];
    }
}
