<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Motoristas.
 *
 * @package namespace App\Entities;
 */
class Motorista extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['nome_completo', 'nome_guerra', 'cpf', 'funcao'];

    public function setNomeGuerraAttribute($value)
    {
        $this->attributes['nome_guerra'] = strtoupper($value);
    }

}
