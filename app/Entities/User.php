<?php

namespace App\Entities;

use Caffeinated\Shinobi\Traits\ShinobiTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Traits\TransformableTrait;
use App\Entities\Service;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, ShinobiTrait, TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function servico()
    {
        return $this->belongsTo(Service::class, 'cmt_user_id');
    }

    public function getFormattedCreatedAtAttribute()
    {
        $create_at = $this->attribute['created_at'];
        return (new \DateTime($create_at))->format('d/m/Y | H:i ');
    }
}
