<?php
/**
 * Created by PhpStorm.
 * User: SGT FRANCISCO
 * Date: 26/07/2018
 * Time: 10:23
 */

namespace App\Services;


use App\Exceptions\ExceptionsErros;
use App\Repositories\MotoristasRepository;

class MotoristasService
{
    protected $repository;
    protected $erros;

    public function __construct(MotoristasRepository $repository, ExceptionsErros $erros)
    {
        $this->repository = $repository;
        $this->erros = $erros;
    }

    public function store($dados)
    {
        try {
            $this->repository->create($dados);

            return [
                'success' => true,
                'messages' => $this->checaFuncao($dados['funcao'])
            ];

        } catch (\Exception $e) {
            return $this->erros->errosExceptions($e);
        }

    }

    public function update($dados, $id)
    {
        try
        {
            $this->repository->update($dados, $id);

            return [
              'success'     => true,
              'messages'    => $this->checaFuncao($dados['funcao'])
            ];

        }catch (\Exception $e)
        {
            return $this->erros->errosExceptions($e);
        }
    }

    public function destroy($id)
    {
        try
        {
            $this->repository->delete($id);

            return [
                'success'   => true,
                'messages'  => 'Motorista | Chefe de Viatura Excluido com Sucesso!'
            ];

        }catch (\Exception $e)
        {
            return $this->erros->errosExceptions($e);
        }
    }

    private function checaFuncao($funcao)
    {
        if ($funcao == 'motorista') {
            return 'Motorista Atualizado com Sucesso!';
        } else {
            return 'Chefe de Viatura Atulizado com Sucesso!';
        }
    }
}