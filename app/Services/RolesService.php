<?php

namespace App\Services;

use App\Exceptions\ExceptionsErros;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

class RolesService
{
    protected $errors;

    public function __construct( ExceptionsErros $errors)
    {
        $this->errors = $errors;
    }

    public function store($data)
    {
        try
        {
            $role = Role::create($data->all());

            $role->permissions()->sync($data->get('permissions'));

            return [
                'success'   => true,
                'messages'  => 'Perfil de Acesso Cadastrado com Sucesso'
            ];

        }catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }

    public function update($data, $id)
    {
        try
        {
            $role = Role::find($id);
            $role->update($data->all());
            
            $role->permissions()->sync($data->get('permissions'));
            
            return [
                'success'   => true,
                'messages'  => 'Perfil de Acesso Atualizado com Sucesso',
            ];
        }
        catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }

    public function destroy($id)
    {
        try
        {
            $role = Role::find($id);
            $role->delete();

            return [
                'success'   => true,
                'messages'  => 'Perfil de Acesso Excluido com Sucesso'
            ];
        }
        catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }
}
