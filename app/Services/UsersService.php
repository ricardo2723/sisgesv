<?php

namespace App\Services;

use App\Exceptions\ExceptionsErros;
use App\Repositories\UserRepository;
class UsersService
{
    private $repository;
    protected $errors;

    public function __construct(UserRepository $repository, ExceptionsErros $errors)
    {
        $this->repository   = $repository;
        $this->errors       = $errors;
    }

    public function update($data, $id)
    {
        try
        {
            $user = $this->repository->update($data->all(), $id);
            
            $user->roles()->sync($data->get('roles'));
            
            return [
                'success'   => true,
                'messages'  => 'Usuário Atualizado com Sucesso',
            ];
        }
        catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }

    public function destroy($id)
    {
        try
        {
            $this->repository->delete($id);

            return [
                'success'   => true,
                'messages'  => 'Usuário Excluido com Sucesso'
            ];
        }
        catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }
}
