<?php

namespace App\Services;

use App\Exceptions\ExceptionsErros;
use App\Repositories\ViaturasRepository;

class ViaturasService
{
    private $repository;
    protected $errors;

    public function __construct(ViaturasRepository $repository, ExceptionsErros $errors)
    {
        $this->repository   = $repository;
        $this->errors       = $errors;
    }

    public function store($data)
    {
        try
        {
            $this->repository->create($data);

            return [
                'success'   => true,
                'messages'   => 'Viatura Cadastrada com sucesso'
            ];
        }
        catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }

    public function update(array $data, $id)
    {
        try
        {
            $this->repository->update($data, $id);

            return [
                'success'   => true,
                'messages'  => 'Viatura Atualizada com Sucesso'
            ];
        }
        catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }

    public function destroy($id)
    {
        try
        {
            $this->repository->delete($id);

            return [
                'success'   => true,
                'messages'  => 'Viatura Excluida com Sucesso'
            ];
        }
        catch(\Exception $e)
        {
            return $this->errors->errosExceptions($e);
        }
    }
}
