<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MotoristasRepository.
 *
 * @package namespace App\Repositories;
 */
interface MotoristasRepository extends RepositoryInterface
{
    //
}
