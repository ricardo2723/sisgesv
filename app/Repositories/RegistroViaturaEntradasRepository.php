<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RegistroViaturaEntradasRepository.
 *
 * @package namespace App\Repositories;
 */
interface RegistroViaturaEntradasRepository extends RepositoryInterface
{
    //
}
