<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RegistroViaturaEntradasRepository;
use App\Entities\RegistroViaturaEntrada;
use App\Validators\RegistroViaturaEntradasValidator;

/**
 * Class RegistroViaturaEntradasRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RegistroViaturaEntradasRepositoryEloquent extends BaseRepository implements RegistroViaturaEntradasRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RegistroViaturaEntrada::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
