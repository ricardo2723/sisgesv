<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RegistroViaturasRepository;
use App\Entities\RegistroViatura;
use App\Validators\RegistroViaturasValidator;

/**
 * Class RegistroViaturasRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RegistroViaturasRepositoryEloquent extends BaseRepository implements RegistroViaturasRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RegistroViatura::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
