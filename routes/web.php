<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

// ############################# USERS #################################################
// Route::get('/login', 'DashboardController@login')->name('user.login');
// Route::post('/login', 'DashboardController@auth')->name('user.logar');
// Route::get('/logout', 'DashboardController@logout')->name('user.logout');

// ############################# SERVIÇOS ###############################################
Route::resource('servico', 'ServicesController');
Route::get('/servico/user/{user}', 'ServicesController@openService')->name('servico.openService');
Route::get('/showCloseService', 'ServicesController@showCloseService')->name('servico.showCloseService');
Route::get('/closeService', 'ServicesController@closeService')->name('servico.closeService');

// ############################### ROTAS AUTENTICADAS ####################################
Route::middleware('auth')->group(function () {

// ################################ HOME #################################################
    Route::get('/home', 'HomeController@index')->name('home');

// ############################# USUARIOS #################################################
    Route::resource('usuarios', 'UsersController');

// ###################################### MOTORISTA #######################################
    Route::resource('motoristas', 'MotoristasController');

// ############################## ACESSOS #################################################
    Route::resource('acessos', 'RolesController');

// ############################# VIATURAS #################################################
    Route::resource('viaturas', 'ViaturasController');

// ############################# REGISTRO DE SAIDA DE VIATURAS #############################
    Route::resource('registroViaturas', 'RegistroViaturasController');
    Route::get('/registroViatura/corrigir/{registroViaturaOficial}', 'RegistroViaturasController@corrigirRegistroShow')->name('registroViatura.corrigirRegistro');
    Route::put('/registroViatura/{registroViaturaOficial}/update', 'RegistroViaturasController@corrigirRegistroUpdate')->name('registroViatura.corrigirRegistroUpdate');
    Route::any('/dashboard/CmtGda', 'RegistroViaturasController@dashboard')->name('registroViatura.dashboard');

// ############################# REGISTRO DE ENTRADA DE VIATURA ############################
    Route::resource('registroViaturaEntradas', 'RegistroViaturaEntradasController');

// ############################# REGISTRO DE VIATURA OFFICAL ###############################
    Route::get('/registroViaturaOficial', 'RegistroViaturaOficialsController@index')->name('registroViaturaOficial.index');
    Route::get('/registroViaturaOficial/{registroViaturaOficial}', 'RegistroViaturaOficialsController@show')->name('registroViaturaOficial.show');
    Route::put('/registroViaturaOficial/{registroViaturaOficial}/edit', 'RegistroViaturaOficialsController@edit')->name('registroViaturaOficial.edit');
    Route::get('/registroViaturaOficial/finalizarRegistro/{finalizarRegistro}', 'RegistroViaturaOficialsController@finalizarRegistro')->name('registroViaturaOficial.finalizarRegistro');
    Route::get('/trocarCmtGda', 'RegistroViaturaOficialsController@trocarCmtGda')->name('registroViaturaOficial.trocarCmtGda');
    Route::put('/trocarCmtGda/{trocarCmtGda}/update', 'RegistroViaturaOficialsController@trocarCmtGdaUpdate')->name('registroViaturaOficial.trocarCmtGdaUpdate');
    Route::any('/dashboard/ofDia', 'RegistroViaturaOficialsController@dashboard')->name('registroViaturaOficial.dashboard');
    Route::any('/registroViaturaOficial/relatorios/pdf', 'RegistroViaturaOficialsController@viaturasPdf')->name('registroViaturaOficial.viaturasPdf');
    Route::any('/registroViaturaOficial/relatorios/pdf/gerar', 'RegistroViaturaOficialsController@gerarViaturasPdf')->name('registroViaturaOficial.gerarViaturasPdf');

// ######################################### GESTOR #########################################
    Route::get('/dashboard/gestor', 'GestorController@dashboard')->name('gestor.dashboard');
    Route::get('/report/viaturas', 'GestorController@dashboardViaturasPdf')->name('gestor.dashboardViaturasPdf');
    Route::get('/report/viaturas/pdf', 'GestorController@dashboardGerarViaturasPdf')->name('gestor.dashboardGerarViaturasPdf');
    Route::get('/report/viaturas/{report}', 'GestorController@viaturasPdf')->name('gestor.viaturasPdf');
    Route::get('/report/viaturas/pdf/{report}', 'GestorController@gerarViaturasPdf')->name('gestor.gerarViaturasPdf');
    // ################### SERVIÇO ###################
    Route::get('/report/servico', 'GestorController@servico')->name('gestor.reportServico');
    Route::post('/report/servicoShow', 'GestorController@servicoShow')->name('gestor.reportServicoShow');
    Route::post('/report/servicoDataShow', 'GestorController@servicoDataShow')->name('gestor.reportServicoDataShow');
    // ################### SERVIÇO OFICIAL ###################
    Route::get('/report/servico/oficial', 'GestorController@servicoOficial')->name('gestor.reportServicoOficial');
    Route::post('/report/servico/oficialShow', 'GestorController@servicoOficialShow')->name('gestor.reportServicoOficialShow');
    // ################### SERVIÇO CMT GDA ###################
    Route::get('/report/servico/cmtGda', 'GestorController@servicoCmtGda')->name('gestor.reportServicoCmtGda');
    Route::post('/report/servico/cmtGdaShow', 'GestorController@servicoCmtGdaShow')->name('gestor.reportServicoCmtGdaShow');
    // ################### VIATURA ###################
    Route::get('/report/viatura', 'GestorController@viatura')->name('gestor.reportViatura');
    Route::post('/report/viaturaShow', 'GestorController@viaturaShow')->name('gestor.reportViaturaShow');
    Route::post('/report/viaturaDataShow', 'GestorController@viaturaDataShow')->name('gestor.reportViaturaDataShow');
});