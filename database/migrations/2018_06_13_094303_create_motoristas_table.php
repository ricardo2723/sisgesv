<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateMotoristasTable.
 */
class CreateMotoristasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('motoristas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nome_completo');
            $table->string('nome_guerra', 45)->unique();
            $table->string('cpf',12)->nullable();
            $table->string('funcao');

            $table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('motoristas');
	}
}
