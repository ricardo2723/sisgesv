<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateViaturasTable.
 */
class CreateViaturasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('viaturas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('prefixo', 45);
            $table->string('eb_placa', 20)->unique();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('viaturas');
	}
}
